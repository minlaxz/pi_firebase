# raspberrypi_firebase_and sensor
'''This Python Script is created by Min Latt (Confu_PENDA) and aimed for
(1)Connection Between OLED Display Hat(SH1106 Driver included Joystick and Keys)
with Raspberry Pi3 Model B+ via Serial SPI Interface <*But I am using Display ONLY>,
(2)To get Data from Humidity And Temperature Sensor on OLED Display
(3)Raspberry Pi' CPU Temperature, Core Voltage, Memory Used 
(4)Dived into one service (Realtime Database) of Google' Firebase
(5)Update Pi Information to Firebase *Realtime
(6)Update Humidity and Temperature of the Sensor to Firebase *Realtime
(7)To get max and min value of Temperature and Humidity of Sensor *Realtime
(8)Using Google Assistance on Raspberry Pi
(9)Controlling GPIO of Raspberry Pi
(like getting signal and controlling of 220V AC)
*On Off Switch using Google Assistance via Voice Control
(10)Now I can switch on and off by just saying "Hey Google, I'm up."
This will turn all bedroom light off and
turn Desktop Computer on
'''
